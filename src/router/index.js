import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/pages',
      name: 'pages',
      component: () => import('../views/Pages.vue'),
      meta: {
        title: 'Castle of Blackwater - All pages'
      }
    },
    
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue'),
      meta: {
        title: 'Castle of Blackwater'
      }
    },
    {
      path: '/character',
      name: 'Character',
      component: () => import('../views/Character.vue'),
      meta: {
        title: 'Castle of Blackwater - Character'
      }
    },
    {
      path: '/inventory',
      name: 'Inventory',
      component: () => import('../views/Inventory.vue'),
      meta: {
        title: 'Castle of Blackwater - Inventory'
      }
    },
    {
      path: '/buy',
      name: 'Buy',
      component: () => import('../views/Buy.vue'),
      meta: {
        title: 'Castle of Blackwater - Buy'
      }
    },
    {
      path: '/mistery-box',
      name: 'Mistery Box',
      component: () => import('../views/MisteryBox.vue'),
      meta: {
        title: 'Castle of Blackwater - Mistery Box'
      }
    },
    {
      path: '/box-single',
      name: 'Box Single',
      component: () => import('../views/BoxSingle.vue'),
      meta: {
        title: 'Castle of Blackwater - Box Single'
      }
    },
    {
      path: '/settings-noaccount',
      name: 'Account Settings_NOACCOUNT',
      component: () => import('../views/SettingsNoaccount.vue'),
      meta: {
        title: 'Castle of Blackwater - Account Settings_NOACCOUNT'
      }
    },
    {
      path: '/account-settings',
      name: 'Account Settings',
      component: () => import('../views/AccountSettings.vue'),
      meta: {
        title: 'Castle of Blackwater - Account Settings'
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('../views/Login.vue'),
      meta: {
        title: 'Castle of Blackwater - Login'
      }
    },
    {
      path: '/staking',
      name: 'Staking',
      component: () => import('../views/Staking.vue'),
      meta: {
        title: 'Castle of Blackwater - Staking'
      }
    },
    {
      path: '/popups-list',
      name: 'Popups List',
      component: () => import('../views/PopupsList.vue'),
      meta: {
        title: 'Castle of Blackwater - Popups List'
      }
    },
  ]
});

router.beforeEach((toRoute, fromRoute, next) => {
  window.document.title = toRoute.meta && toRoute.meta.title ? toRoute.meta.title : 'Home';

  next();
})

export default router
