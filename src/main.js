import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './assets/fonts/fonts.css'
import './index.css'

import './assets/main.css'

const app = createApp(App)

app.mixin({
    methods: {
        randomStr(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
              result += characters.charAt(Math.floor(Math.random() * charactersLength));
           }
           return result;
        }
    }
})

app.use(router)

app.mount('#app')
