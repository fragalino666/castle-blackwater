/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'bg_primary': '#0F0517',
        'gray': '#7A7379',
        'blue': '#842FDE',
        'light_blue': '#7C24F5',
        'gold': '#D7C181',
        'opacity_white': 'rgba(255,255,255,0.1)'
      },
      screens: {
        'xl+': "1400px"
      }
    },
  },
  plugins: [],
}
